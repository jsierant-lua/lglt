local class = require('classy')
local sleep = require('sleep')

local function get_arg(prefix, arg)
  if arg then return string.format(prefix, arg) end
  return ''
end


local Glt = class('Glt')

Glt.color = require('lglt.lglt.color');


function Glt:__init()
  self._gnuplot = assert(io.popen('gnuplot --persist', 'w'))
  self._data_next_id = 0
end

function Glt:_define_data(args)
  local data_name = string.format('$data_set%s', self._data_next_id)
  self:write(data_name .. ' << EOD\n')
  self._data_next_id = self._data_next_id + 1
  for idx=1,#args.x do
    self:write(string.format('%s %s\n', args.x[idx], args.y[idx]))
  end
  self:write('EOD\n')
  return data_name
end

function Glt:raw(fmt, ...)
  self:write(string.format(fmt, ...), '\n')
end

function Glt:plot(...)
  local args = {}
  for _,styled_data in ipairs({...}) do
    local data_var = self:_define_data(styled_data[1])
    table.insert(args, string.format('%s using 1:2 %s', data_var, table.concat(styled_data[2], ' ')))
  end
  self:write('plot ' .. table.concat(args, ', ') .. '\n')
end

function Glt:set(var, fmt, ...)
  self:raw('set ' .. var .. ' ' .. string.format(fmt or '', ...), '\n')
end

function Glt:set_title(text)
  self:raw('set title %q', text)
end

function Glt:write(text)
  print(text)
  self._gnuplot:write(text)
  self._gnuplot:flush()
end

function Glt:set_label(axis, title)
  self:raw('set %slabel %q', axis, title)
end

-- set grid xtics lt 0 lw 1 lc rgb "#0000ff"

Glt.grid = {
  ytics = function(args)
    return 'ytics ' .. table.concat({
           get_arg('lc rgb %q', args.color),
           get_arg('lt %q', args.style),
           get_arg('lw %d', args.width)
    })
  end,
  y2tics = function(args)
    return 'y2tics ' .. table.concat({
           get_arg('lc rgb %q', args.color),
           get_arg('lt %q', args.style),
           get_arg('lw %d', args.width)
    })
  end
}

function Glt:set_grid(options)
  self:raw('set grid %s', options or '')
end


function Glt:unset(args)
  self:raw('unset %s', args)
end

function Glt:close()
  self:write('quit\n')
  self._gnuplot:close()
end

function Glt:multiplot(args)
  self:write(string.format('set multiplot layout %d,%d\n',
                           #args, #args[1]))
  self._gnuplot:flush()
  sleep(500)  -- wait for window to open

  for _,row_plots in ipairs(args) do
    for _,plot in ipairs(row_plots) do
      plot(self)
      self:unset('title')
      self:unset('grid')
    end
  end
  self:write('unset multiplot\n')
end

Glt.axis = {
  x1y1 = 'x1y1',
  x1y2 = 'x1y2'
}

-- @param[opt] args.color line color (lt rgb "|args.color|")
-- @param[opt] args.style line style (dt "|args.style|")
-- @param[opt] args.width line width (lw "|args.width|")
-- @param[opt] args.name line label (title "|args.name|")
Glt.line = function(args)
  return {{x = args.x, y = args.y},
         { 'with lines',
           get_arg('lc rgb %q', args.color),
           get_arg('dt %q', args.style),
           get_arg('lw %d', args.width),
           get_arg('title %q', args.name or ''),
           get_arg('axis %s', args.axis)
           }}
end

Glt.points = function(args)
  return {{x = args.x, y = args.y},
         { 'with points',
           get_arg('lc rgb %q', args.color),
           get_arg('pt %q', args.style),
           get_arg('title %q', args.name or ''),
           get_arg('axis %s', args.axis)
           }}
end

Glt.lines_points = function(args)
  return {{x = args.x, y = args.y},
         { 'with linespoints',
           get_arg('lc rgb %q', args.color),
           get_arg('dt %q', args.line_style),
           get_arg('lw %d', args.line_width),
           get_arg('pointtype %q', args.point_style),
           get_arg('pointsize %q', args.point_size),
           get_arg('title %q', args.name or ''),
           get_arg('axis %s', args.axis)
           }}
end

return Glt
