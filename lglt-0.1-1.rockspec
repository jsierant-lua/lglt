package = 'lglt'
version = '0.1-1'
source = { url = 'git+https://gitlab.com/jsierant-lua/lglt.git' }
description = {
  summary = 'A simple interface for gnuplot',
  homepage = 'https://gitlab.com/jsierant-lua/lglt',
  license = 'The MIT License (MIT)'
}

supported_platforms = { 'linux' }
dependencies = {
  'sleep >= 1.0.0',
  'classy >= 0.4'
}
build = {
   type = "builtin",
   modules = {
      lglt = 'lglt.lua',
      ['lglt.color'] = 'lglt/color.lua',
   }
}
