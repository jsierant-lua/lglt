local Glt = require('lglt')

local x = {1, 2, 3}
local y1 = {7, 3, 2}
local y2 = {1, 6, -1}

do
  print('plot single line with blue dots')
  local p = Glt{}
  p:plot(Glt.line{x = x, y = y1, color = 'blue', name = 'set1', style = '.' })
  io.read()
  p:close()
end

do
  print('plot multiple lines with different style and color')
  local p = Glt{}
  p:plot(Glt.line{x = x, y = y1, color = 'blue', name = 'set1', style = '.' },
         Glt.line{x = x, y = y2, color = 'green', name = 'set2', style = '-' })
  io.read()
  p:close()
end

do
  print('plot multiple data with different style - line and points')
  local p = Glt{}
  p:plot(Glt.line{x = x, y = y1, color = 'blue', name = 'set1'},
         Glt.points{x = x, y = y2, color = 'red', name = 'set2', style = '#' })
  io.read()
  p:close()
end


do
  print('multiplot')
  local glt = Glt{}
  glt:multiplot{{
    function(sp)
--       sp:set('title', 'multiplot test')
      sp:set('grid')
      sp:plot(Glt.line{x = x, y = y2, color = 'blue', name = 'set1' },
              Glt.line{x = x, y = y1, color = 'green', name = 'set2'})

    end},
    {function(sp)
      sp:plot(Glt.points{x = x, y = y1, color = 'blue', name = 'set1',
                         style = '#' },
              Glt.lines_points{x = x, y = y2, color = 'red', name = 'set2',
                               point_style = '#' })

    end
  }}
  io.read()
  glt:close()
end

do
  print('multiple yaxis')
  local p = Glt{}
  p:raw('set ytics nomirror')
  p:raw('set y2tics nomirror')
  p:set_grid(Glt.grid.ytics{color = Glt.color.dark.magenta})
  p:set_grid(Glt.grid.y2tics{color = Glt.color.dark.yellow})

  p:plot(Glt.line{x = x, y = y1, color = 'blue', name = 'set1', axis = Glt.axis.x1y1},
         Glt.line{x = x, y = y2, color = 'red', name = 'set2', axis = Glt.axis.x1y2})
  io.read()
  p:close()
end
